import logo from './logo.svg';
import './App.css';
import { useEffect, useState} from 'react'
import Chacarcters from './components/Chacarcters';

function App() {

  const [characterList, setCharacterList] = useState([])

  useEffect(()=>{
    fetch("https://rickandmortyapi.com/api/character")
    .then((x) => x.json())
    .then((x) => setCharacterList(x.results))
    .catch(console.log('erro'))
  }, [])

  return (
    <div className="App">
      <main className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <Chacarcters charList={characterList}></Chacarcters>
      </main>
    </div>
  );
}

export default App;
