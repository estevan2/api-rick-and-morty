import CharCard from "../CharCard"
import "./style.css"

export default function Chacarcters({ charList }) {

    return (
        <div>
            <h1>Meus Personagens</h1>
            <div className="charList">
                {charList.map((item, index)=>(
                    <CharCard name={item.name} status={item.status} image={item.image} id={item.id} key={index}></CharCard>
                ))}
            </div>
        </div>
    )
}