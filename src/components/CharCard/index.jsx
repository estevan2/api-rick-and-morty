import "./style.css"

export default function CharCard({name, status, image, id}){
    
    return (
        <div className={status} key={id}>
            <h3>{name}</h3>
            <figure>
                <img src={image} alt={name+ " portrait"} />
            </figure>
        </div>
    )
}